var canvas = document.getElementById("ctx");
var ctx = canvas.getContext("2d");

var WIDTH = canvas.clientWidth;
var HEIGHT = canvas.clientHeight;

canvas.width = WIDTH;
canvas.height = HEIGHT;

var score = 0;
var frameCount = 0;
var timeStart = Date.now();

var skeletonImage = new Image();
skeletonImage.src ="images/hero.png";

var enemyImage = new Image();
enemyImage.src = "images/skeleton.png";

var background = new Image();
background.src = "images/background.png";

var fireImage = new Image();
fireImage.src = "images/fireball.png";

var stoneImage = new Image();
stoneImage.src = "images/stone.png";

var bloodImage = new Image();
bloodImage.src = "images/blood.png";

var enemyList = {};
var bulletList = {};
var potionList = {};
var bloodList = {};
var bestScores = {};

/* *** FIREBASE ***/
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyDyu7n_Jt7dyhalcb6yjm64jLmWdDu73w0",
    authDomain: "haunted-dungeon.firebaseapp.com",
    databaseURL: "https://haunted-dungeon.firebaseio.com",
    projectId: "haunted-dungeon",
    storageBucket: "haunted-dungeon.appspot.com",
    messagingSenderId: "946481303514",
    appId: "1:946481303514:web:903887cb3ba2b174bbbae3"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

//zapisywanie wyników do bazy danych
function writeUserData(name, score) {
    firebase.database().ref("scores/").push({
        name: name,
        score: score,
    });
}
//odczytywanie wyników
var scoresRef = firebase.database().ref("scores");
scoresRef.orderByChild("score").limitToLast(3).on("value", function(snapshot) {
    snapshot.forEach(function(data) {
      bestScores[data.val().name] = data.val().score;
    });
});
/* ***KONIEC FIREBASE ***/

/* ***KLASY*** */
class Player{
    constructor(){
        this.x = WIDTH * 0.4;
        this.y = HEIGHT * 0.4;
        this.speedX = 0;
        this.speedY = 0;
        this.width = 60;
        this.height = 65;
        this.angle = 90;
        this.shotAngle = 90;
        this.mana = 100;
        this.hp = 100;
        this.dir = 0;
        this.step = 0;
    }
    draw(i){
        if(player.speedX == 0 && player.speedY == 0)
            i = 0;
        if(player.angle <= 135 && player.angle > 45)
            this.dir = 0;
        else if(player.angle < -135 || player.angle > 135)
            this.dir = 1;
        else if(player.angle >= -135 && player.angle < -45)
            this.dir = 2;
        else 
            this.dir = 3;

        ctx.save();
        ctx.translate(player.x, player.y);
        //ctx.rotate(Math.PI/(step%4));
        ctx.rotate(Math.radians(this.step%360));
        ctx.translate(-player.x, -player.y);

        ctx.drawImage(
            skeletonImage, i*60, this.dir*65,
            this.width, this.height,
            this.x, this.y,
            this.width, this.height);
            
        ctx.restore();
    }
    fire(type){
        var x = this.x + 25;
        var y = this.y + 20;
        var id = Math.random();
        var angle = this.shotAngle;
        var spdX = Math.cos(angle/180*Math.PI) * 5;
        var spdY = Math.sin(angle/180*Math.PI) * 5;

        if(type=='stone'){
            bulletList[id] = new Bullet(id, x, y, spdX, spdY, 10, 10, false, 'stone');
        }
        else{
            if (this.mana >= 10){
                bulletList[id] = new Bullet(id, x, y, spdX, spdY, 10, 10, false, 'fire');
                this.mana-=10;
            }
        }
    }
}

class Enemy{
    constructor(id, x, y, width, height, image, frameWhenBorn){
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.image = image;
        this.frameWhenBorn = frameWhenBorn;
        this.hp = 5;
        this.steps = 0;
        this.speedX = 0;
        this.speedY = 0;
        this.a = 0;
        this.dir = 2;
    }
    attack(){
        var angle = Math.atan2(player.y - this.y, player.x - this.x) * 180 / Math.PI;
        var bulletId = Math.random();
        var spdX = Math.cos(angle/180*Math.PI) * 5;
        var spdY = Math.sin(angle/180*Math.PI) * 5;
        bulletList[bulletId] = new Bullet(bulletId, this.x + 25, this.y + 20, spdX, spdY, 10, 10, true);
    }
    draw(){
        if(this.speedX > 0) this.dir = 3;
        if(this.speedX < 0) this.dir = 1;
        if(this.speedY > 0) this.dir = 2;
        if(this.speedY < 0) this.dir = 0;
        ctx.drawImage(this.image, this.a * 64, this.dir * 64, this.width, this.height, this.x, this.y, this.width, this.height);
    }
    generatePotion(){
        var id = Math.random();
        var height = 10;
        var width = 10;
        var type = "";
        var rand = Math.random();
        if(rand<0.5) type = "mana";
        else if(rand>=0.5) type = "health";
    
        potionList[id] = new Potion(id, this.x, this.y, height, width, type);
    }
    move(sX, sY){
        this.steps = 20;
        this.speedX = sX;
        this.speedY = sY;
    }
}

class Bullet{
    constructor(id, x, y, spdX, spdY, width, height, isEnemy, type){
        this.id = id;
        this.x = x;
        this.y = y;
        this.spdX = spdX;
        this.spdY = spdY;
        this.width = width;
        this.height = height;
        this.isEnemy = isEnemy;
        this.type = type;
    }
    draw(){
        ctx.save();
        if(this.type == 'stone'){
            ctx.drawImage(stoneImage, this.x, this.y, this.width, this.height);
        }
        else{
            ctx.drawImage(fireImage, this.x, this.y, this.width, this.height);
        }
        ctx.restore();
    }
    updatePosition(){
        this.x += this.spdX;
        this.y += this.spdY;
    }
}

class Potion{
    constructor(id, x, y, width, height, type){
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.type = type;
    }
    draw(){
        ctx.save();
        if (this.type == "mana") ctx.fillStyle = 'blue';
        else if (this.type == "health") ctx.fillStyle = 'red';
        ctx.fillRect(this.x - this.width/2, this.y - this.height/2, this.width, this.height);
        ctx.restore();    
    }
}

class Blood{
    constructor(id, x, y){
        this.id = id;
        this.x = x;
        this.y = y;
        this.width = 50
        this.height = 50;
    }
    draw(){
        //ctx.save();
        ctx.drawImage(bloodImage, this.x, this.y, this.width, this.height);
        //ctx.restore();
    }
}
/* ***KONIEC KLAS*** */

var player = new Player();

//Rysowanie tła
function drawBackground(ctx){
    ctx.drawImage(background, 0, 0, WIDTH, HEIGHT);
}

/* ***PRZYCISKI*** */
function drawVJoy(ctx){
    ctx.strokeStyle="#FFFFFF"; 
    ctx.beginPath();
    ctx.ellipse(vjoy.centerX, vjoy.centerY, vjoy.radius, vjoy.radius, 1, 0, 2*Math.PI)
    ctx.stroke();    
        
    if (Math.pow(Math.pow(vjoy.centerX - vjoy.pointX, 2) + Math.pow(vjoy.centerY - vjoy.pointY, 2), 0.5) < vjoy.radius){
        ctx.fillStyle="#00fafa";
        ctx.beginPath();
        ctx.ellipse(vjoy.pointX, vjoy.pointY, 0.15 * vjoy.radius, 0.15 * vjoy.radius, 1, 0,2 * Math.PI)
        ctx.fill();
    }

    ctx.fillStyle="#FFFFFF"; 
    ctx.beginPath();
    ctx.ellipse(vjoy.centerX, vjoy.centerY, 0.1 * vjoy.radius, 0.1 * vjoy.radius, 1, 0, 2 * Math.PI)
    ctx.fill(); 
}

function drawVJoy2(ctx){
    ctx.strokeStyle="#FFFFFF"; 
    ctx.beginPath();
    ctx.ellipse(vjoy2.centerX, vjoy2.centerY, vjoy2.radius, vjoy2.radius, 1, 0, 2*Math.PI)
    ctx.stroke();    
        
    if (Math.pow(Math.pow(vjoy2.centerX - vjoy2.pointX, 2) + Math.pow(vjoy2.centerY - vjoy2.pointY, 2), 0.5) < vjoy2.radius){
        ctx.fillStyle="#00fafa";
        ctx.beginPath();
        ctx.ellipse(vjoy2.pointX, vjoy2.pointY, 0.15 * vjoy2.radius, 0.15 * vjoy2.radius, 1, 0,2 * Math.PI)
        ctx.fill();
    }

    ctx.fillStyle="#FFFFFF"; 
    ctx.beginPath();
    ctx.ellipse(vjoy2.centerX, vjoy2.centerY, 0.1 * vjoy2.radius, 0.1 * vjoy2.radius, 1, 0, 2 * Math.PI)
    ctx.fill(); 
}

function drawStoneButton(ctx){
    ctx.save();
    ctx.fillStyle="#8D8080"; 
    ctx.beginPath();
    ctx.ellipse(stoneButton.centerX, stoneButton.centerY, stoneButton.radius, stoneButton.radius,1,0,2*Math.PI)
    ctx.fill();
    ctx.restore();    
}

function drawFireButton(ctx){
    ctx.save();
    ctx.fillStyle="#ff0000"; 
    ctx.beginPath();
    ctx.ellipse(fireButton.centerX, fireButton.centerY, fireButton.radius, fireButton.radius,1,0,2*Math.PI)
    ctx.fill();
    ctx.restore();    
}
/* ***KONIEC PRZYCISKOW*** */

//czy punkt należy do okręgu
function pointInCircle(x, y, cx, cy, radius) {
    var distancesquared = (x - cx) * (x - cx) + (y - cy) * (y - cy);
    return distancesquared <= radius * radius;
}

Math.radians = function (degrees)
{
    return degrees * Math.PI / 180;
};

//kolizja dwóch prostokątów
function testCollisionRectRect(rect1,rect2){
    return rect1.x <= rect2.x + rect2.width
            && rect2.x <= rect1.x + rect1.width
            && rect1.y <= rect2.y + rect2.height
            && rect2.y <= rect1.y + rect1.height;
}

//kolizja dwóch jednostek
function testCollisionEntity(entity1, entity2){
    var rect1 = {
        x: entity1.x,
        y: entity1.y,
        width: entity1.width,
        height: entity1.height
    };
    var rect2 = {
        x: entity2.x,
        y: entity2.y,
        width: entity2.width,
        height: entity2.height
    };
    return testCollisionRectRect(rect1, rect2);
}

//generowanie przeciwnika
function randomlyGenerateEnemy(frameCount){
    var id = Math.random();
    var x;
    var y;
    var height = 64;
    var width = 64;
    do{
        x = Math.random() * WIDTH - width;
        y = Math.random() * HEIGHT - height;
    }while (x < 0 || x-width > WIDTH || y < 0.1*HEIGHT || y-height > HEIGHT);

    enemyList[id] = new Enemy(id, x, y, width, height, enemyImage, frameCount);
}

//sprawdzanie czy gracz nie wychodzi poza pole gry
function checkBoundaries(entity){
    if (entity.x < 0 || entity.x > (WIDTH-entity.width)){
        entity.speedX=0;
        entity.speedY=0;
    }
    if (entity.y< 0.1* HEIGHT || entity.y>(HEIGHT-entity.height)){
        entity.speedX=0;
        entity.speedY=0;
    }
}

//krew
function createBlood(x, y){
    var id = Math.random();
    bloodList[id] = new Blood(id, x, y);
}

//sprawdzanie wszystkich kolizji
function colisionDetection(){
    //przeciwnicy
    for(var przeciwnik in enemyList){
        var isColiding = testCollisionEntity(player, enemyList[przeciwnik]);
        if (isColiding){
            player.speedX = 0;
            player.speedY = 0;
            enemyList[przeciwnik].speedX = 0;
            enemyList[przeciwnik].speedY = 0;
        }
    }

    //pociski
    for(var bullet in bulletList){
        var toRemove = false;
        for(var enemy in enemyList){
            var isColiding = testCollisionEntity(bulletList[bullet], enemyList[enemy]);
            if (isColiding && bulletList[bullet].isEnemy == false){
                if (bulletList[bullet].type == 'stone'){
                    enemyList[enemy].hp--;
                    toRemove = true;
                    if(enemyList[enemy].hp <= 0){
                        enemyList[enemy].generatePotion();
                        delete enemyList[enemy];
                        score++;
                        break;
                    }
                }
                else{
                    enemyList[enemy].generatePotion();
                    delete enemyList[enemy];
                    toRemove = true;
                    score++;
                    break;
                }
            }
        }
        var isColiding = testCollisionEntity(player, bulletList[bullet]);
        if (isColiding && bulletList[bullet].isEnemy == true){
            player.hp -= 5;
            //createBlood(player.x, player.y);
            player.step = 1;
            toRemove = true;
        }
        if (toRemove) delete bulletList[bullet];
    }

    //mikstury
    for(var potion in potionList){
        var isColiding = testCollisionEntity(player, potionList[potion]);
        if (isColiding){
            if (potionList[potion].type == "mana"){
                player.mana += 20;
                if (player.mana > 100) player.mana = 100;
            }
            else if (potionList[potion].type == "health"){
                player.hp += 20;
                if (player.hp > 100) player.hp = 100;
            }
            delete potionList[potion];
        }
    }
}
var step = 0;

//animacje
function animation(){
    ctx.clearRect(0,0,canvas.width, canvas.height);
    drawBackground(ctx);

    for(var key in bloodList)
        bloodList[key].draw();
    
    //animacja postaci gracza
    var i = frameCount % 9;
    player.draw(i);

    //rysowanie przycisków
    drawVJoy(ctx);
    drawVJoy2(ctx);
    drawStoneButton(ctx);
    drawFireButton(ctx);
    
    //rysowanie przeciwników
    var a = frameCount % 8;
    for(var key in enemyList){
        enemyList[key].draw();
        enemyList[key].a = a;
    }

    //rysowanie pocisków
    for(var key in bulletList){
        bulletList[key].updatePosition();
        bulletList[key].draw();
    }

    //rysowanie mikstur
    for(var key in potionList)
        potionList[key].draw();

    //rysowanie wyniku na ekranie
    ctx.font = "18px Arial";
    ctx.fillText("Score: " + score, WIDTH - 100, 20);

    //rysowanie pasków HP i many
    ctx.font = "18px Arial";
    ctx.save();
    ctx.fillText("HP: ", 5, 20, 50);
    ctx.fillStyle = "red";
    ctx.fillRect(40, 5, player.hp, 18);
    ctx.strokeRect(40, 5, 100, 18);
    ctx.restore();

    ctx.font = "18px Arial";
    ctx.fillText("MP: ", 5, 50, 50);
    ctx.fillStyle = "blue";
    ctx.fillRect(40, 35, player.mana, 18);
    ctx.strokeRect(40, 35, 100, 18);
    
    if (player.hp>0)
        requestAnimationFrame(animation);
}

//aktualizacja stanów
function update(){
    frameCount++;
    player.x+=player.speedX;
    player.y+=player.speedY;

    //player.step+=10;
    if(player.step>0 && player.step < 360){
        player.step += 10;
    }
    else{
        player.step = 0;
    }

    checkBoundaries(player);
    colisionDetection();

    //zmiana czestotliwosci generowania sie przeciwnikow
    var dif;
    var elapsedTime = Date.now() - timeStart;
    if (elapsedTime < 30000) dif = 150;
    if (elapsedTime >= 30000 && elapsedTime < 45000) dif = 120;
    if (elapsedTime >= 45000 && elapsedTime < 60000) dif = 100;
    if (elapsedTime >= 60000) dif = 50;
    if (frameCount % dif == 0)  randomlyGenerateEnemy(frameCount);

    //atak i ruch przeciwników
    for(var przeciwnik in enemyList){
        enemyList[przeciwnik].frameWhenBorn++;
        checkBoundaries(enemyList[przeciwnik]);

        if(enemyList[przeciwnik].steps > 0){
            enemyList[przeciwnik].x += enemyList[przeciwnik].speedX;
            enemyList[przeciwnik].y += enemyList[przeciwnik].speedY;
            enemyList[przeciwnik].steps--;
        }
        if(enemyList[przeciwnik].frameWhenBorn % 45 == 0)
            enemyList[przeciwnik].attack();
        if(enemyList[przeciwnik].frameWhenBorn % 50 == 0){
            var rand = Math.random();
            if (rand < 0.25 )
                enemyList[przeciwnik].move(-1, 0);
            else if (rand >= 0.25 < 0.5)
                enemyList[przeciwnik].move(0, -1);
            else if (rand >= 0.5 < 0.75)
                enemyList[przeciwnik].move(1, 0);
            else
                enemyList[przeciwnik].move(0, 1);
        }
    }
    if (player.hp>0) 
        setTimeout(update,50);
    else{
        var scores = "";
        for(var key in bestScores){
            scores += key + ": " + bestScores[key] + "\n";
        }
        var person = prompt("Najlepsze wyniki: \n"+scores+"\nWpisz twoje imie", "Twoje imie");
        if (person != null || person != "")
            writeUserData(person, score);
    }
}

/* ***STEROWANIE*** */
vjoy = {
    pointX: null,
    pointY: null,
    centerX: 0.1 * WIDTH,
    centerY: 0.8 * HEIGHT,
    radius: 50
}

vjoy2 = {
    pointX: null,
    pointY: null,
    centerX: 0.9 * WIDTH,
    centerY: 0.8 * HEIGHT,
    radius: 50
}

stoneButton = {
    x: 0.9 * WIDTH,
    y: 0.9 * HEIGHT,
    centerX: 0.8 * WIDTH,
    centerY: 0.87 * HEIGHT,
    radius: 25,
}

fireButton = {
    x: 0.9 * WIDTH,
    y: 0.8 * HEIGHT,
    centerX: 0.8 * WIDTH,
    centerY: stoneButton.centerY - 2 * stoneButton.radius - 10,
    radius: 25,
}

canvas.addEventListener('touchstart', klik);

function klik(ev){
    var x = ev.touches[0].clientX;
    var y = ev.touches[0].clientY;

    if (pointInCircle(x, y, vjoy.centerX, vjoy.centerY, vjoy.radius)){
        vjoy.pointX = x;
        vjoy.pointY = y;

        player.speedX = (vjoy.pointX - vjoy.centerX) * 0.15;
        player.speedY = (vjoy.pointY - vjoy.centerY) * 0.15;

        player.angle = Math.atan2(vjoy.pointY - vjoy.centerY, vjoy.pointX - vjoy.centerX) * 180 / Math.PI;
    }
    if (pointInCircle(x, y, stoneButton.centerX, stoneButton.centerY, stoneButton.radius))
        player.fire('stone');
    if (pointInCircle(x, y, fireButton.centerX, fireButton.centerY, fireButton.radius))
        player.fire('fire');
    if (pointInCircle(x, y, vjoy2.centerX, vjoy2.centerY, vjoy2.radius)){
        vjoy2.pointX = x;
        vjoy2.pointY = y;
        
        player.shotAngle = Math.atan2(vjoy2.pointY - vjoy2.centerY, vjoy2.pointX - vjoy2.centerX) * 180 / Math.PI;
    }
}

canvas.addEventListener('touchend', mouseup);

function mouseup(){
    player.speedX = 0;
    player.speedY = 0;
}
/* ***KONIEC STEROWANIA*** */

window.onload = function(){
    randomlyGenerateEnemy(frameCount);
    animation();
    update();
}